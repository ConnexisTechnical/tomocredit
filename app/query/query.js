
const get = (dbName,criteria, callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.findById(criteria, (err, info) => {
        if (err) callback(err);
        else callback(null, info);
    });
};

const create = (dbName,dataToSave, callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.create(dataToSave, (err, info) => {
        if (err) callback(err);
        else callback(null, info);
    });
};

const getMultiple = (dbName,criteria, project,callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.find(criteria, project,{},(err, info) => {
        if (err) callback(err);
        else callback(null, info);
    });
};

const update = (dbName,criteria, dataToSave, option, callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.findOneAndUpdate(criteria, dataToSave, option, (err, info) => {
        if (err) callback(err);
        else callback(null, info);
    });
};
const getPopulate = (dbName,criteria, project, options, populateArray, callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.find(criteria, project, options).lean().populate(populateArray).exec((err, info) => {
        if (err) callback(err);
        else {
            try{
            if(populateArray.indexOf("company_id") > -1){
                for(let i=0;i<info.length;i++){
                    if(info[i].company_id&& info[i].company_id.password){
                        delete info[i].company_id.password
                    }
                }
            }
            callback(null, info)
            }
            catch(err){
                callback(err);
            }
        };
    });
};

const getPopulateOne = (dbName,criteria, project, options, callback) => {
   
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.findById(criteria, project, options)
        .populate({ path: 'Bank', model: 'Bank'})
        .populate({ path: 'Crypto', model: 'Crypto'})
        .populate({ path: 'E_commerce', model: 'E_Commerce'})
        .populate({ path: 'Investment', model: 'Investments'})
        .populate({ path: 'Skills', model: 'Skills'})
        .populate({ path: 'Employee', model: 'Employee'})
        .exec((err, info) => {
        if (err) callback(err);
        else {
            if (info && info.company_id) {
                delete info.company_id.password
            }
            callback(null, info)
        };
  
    });
};

const deleteById = (dbName,criteria, callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.deleteOne(criteria, (err, info) => {
        if (err) callback(err);
        // else callback(null, info);
    });
};

const insertMany = (dbName,dataToSave, callback) => {
    const dataBaseName = require('../models/'+dbName);
    dataBaseName.insertMany(dataToSave, (err, info) => {
        if (err) callback(err);
        else callback(null, info);
    });
};


module.exports = {
    get,
    getMultiple,
    create,
    update,
    getPopulate,
    deleteById,
    getPopulateOne,
    insertMany
};