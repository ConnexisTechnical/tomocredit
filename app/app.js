const express = require('express');
const bodyParser = require('body-parser');
const validator = require('express-validator');
// const e_commerce = require('../app/routes/E_Commerce');
const crypto  = require('../app/routes/Crypto');
const Bank  = require('../app/routes/Bank');
const ecommerce  = require('../app/routes/E_Commerce');
const employee  = require('../app/routes/Employee');
const Investments  = require('../app/routes/Investments');
const skills  = require('../app/routes/Skills');
const users  = require('../app/routes/users');
//database connection
require('./utils/db');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization, x-access-token");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.header("x-access-token", "*");
    next();
});

app.use(validator());
// app.use('/',e_commerce);
app.use('/api/v1/crypto',crypto);
app.use('/api/v1/Bank/',Bank);
app.use('/api/v1/e_commerce/',ecommerce);
app.use('/api/v1/employee/',employee);
app.use('/api/v1/Investments/',Investments);
app.use('/api/v1/skills/',skills);
app.use('/api/v1/users/',users)

let port = 9000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});
