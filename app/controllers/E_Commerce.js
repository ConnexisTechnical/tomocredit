const response = require('../utils/response.js');
const Constants = require('../utils/consts.js');
const query = require('../query/query.js');


const createE_Commerce = async (req, res) => {
    try {
        if(!req.body){
            response.sendErrorCustomMessage(res,'Please enter message display data',Constants.ERROR_500)
        }
        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            const regional_adjustment_data=req.body;
            const create_regional_adjustment=await  createE_CommerceInDb(regional_adjustment_data,res);

        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};


const createE_CommerceInDb=(data,resp)=>{
    let dataToSave={}
    let final_result=[]

    return new Promise((resolve,reject)=>{
            if(data){
                dataToSave=data;
            }
            query.create('E_Commerce',dataToSave,(err,result)=>{
                if(err){
                    reject(err)
                }else{
                    response.sendsuccessData(resp,'success',result)
                    resolve(result)
                }

        })
    });

};

const getE_CommerceDetails = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            query.getMultiple('E_Commerce',{},{},(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};

const getE_CommerceById = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {
            let criteria = {
                id: req.params.id
            };

            query.get('E_Commerce',req.params.id,(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};



module.exports = {
    createE_Commerce,
    getE_CommerceDetails,
    getE_CommerceById
};
