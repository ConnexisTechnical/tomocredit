const response = require('../utils/response.js');
const Constants = require('../utils/consts.js');
const query = require('../query/query.js');


const createCrypto = async (req, res) => {
    try {
        if(!req.body){
            response.sendErrorCustomMessage(res,'Please enter message display data',Constants.ERROR_500)
        }
        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            const regional_adjustment_data=req.body;
            const create_regional_adjustment=await  createCryptoInDb(regional_adjustment_data,res);

        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};


const createCryptoInDb=(data,resp)=>{
    console.log('.................the data is..................',data)
    let dataToSave={}
    let final_result=[]

    return new Promise((resolve,reject)=>{
            if(data){
                dataToSave=data;
            }
            query.create('Crypto',dataToSave,(err,result)=>{
                if(err){
                    reject(err)
                }else{
                    response.sendsuccessData(resp,'success',result)
                    resolve(result)
                }

        })
    });

};

const getCryptoDetails = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            query.getMultiple('Crypto',{},{},(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};

const getCryptoById = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {
            let criteria = {
                id: req.params.id
            };

            query.get('Crypto',req.params.id,(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};


module.exports = {
    createCrypto,
    getCryptoDetails,
    getCryptoById
};