const response = require('../utils/response.js');
const Constants = require('../utils/consts.js');
const query = require('../query/query.js');


const createSkills = async (req, res) => {
    try {
        if(!req.body){
            response.sendErrorCustomMessage(res,'Please enter message display data',Constants.ERROR_500)
        }
        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            const regional_adjustment_data=req.body;
            const create_regional_adjustment=await  createSkillsInDb(regional_adjustment_data,res);

        }
    }catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};


const createSkillsInDb=(data,resp)=>{
    let dataToSave={}

    return new Promise((resolve,reject)=>{
            if(data){
                dataToSave=data;
            }
            query.create('Skills',dataToSave,(err,result)=>{
                if(err){
                    reject(err)
                }else{
                    response.sendsuccessData(resp,'success',result)
                    resolve(result)
                }

        })
    });

};

const getSkillsDetails = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            query.getMultiple('Skills',{},{},(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};

const getSkillsById = async (req, res) => {
    try {
        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {
            query.get('Skills',req.params.id,(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};





module.exports = {
    createSkills,
    getSkillsDetails,
    getSkillsById
};