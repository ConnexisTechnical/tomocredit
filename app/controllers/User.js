const response = require('./../utils/response.js');
const Constants = require('./../utils/consts.js');
const query = require('./../query/query.js');

const createUser = async (req, res) => {
    try {
        if(!req.body){
            response.sendErrorCustomMessage(res,'Please enter message display data',Constants.ERROR_500)
        }
        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            const regional_adjustment_data=req.body;
            const create_regional_adjustment=await  createUser1(regional_adjustment_data,res);

        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};


const createUser1=(data,resp)=>{
    let dataToSave={}

    return new Promise((resolve,reject)=>{
            if(data){
                dataToSave=data;
            }
            query.create('User',dataToSave,(err,result)=>{
                if(err){
                    reject(err)
                }else{
                    response.sendsuccessData(resp,'success',result)
                    resolve(result)
                }

        })
    });

};

const getUserDetails = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            query.getMultiple('User',{},{},(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};

const getUserById = async (req, res) => {
    try {

        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {
            let criteria = {
                id: req.params.id
            };

            query.get('User',req.params.id,(err,result)=>{
                if(err){
                    response.sendErrorMessage(res, Constants.ERROR_500);

                }else{
                    response.sendsuccessData(res,'success',result)

                }
            })
        }
    }catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};

const updateUser = async (req, res) => {
    try {
        if(!req.params.id){
            response.sendErrorCustomMessage(res,'Please enter message_display data',Constants.ERROR_500)
        }
        const errors = req.validationErrors();
        if (errors) {
            let error = '';
            errors.forEach(element => {
                error = error + ' ' + element.msg;
            });
            response.sendErrorCustomMessage(res, error, Constants.ERROR_500);
        } else {

            const regional_adjustment_data=req.body;
            const create_regional_adjustment=await  editUser(regional_adjustment_data);
            if(create_regional_adjustment){
                response.sendsuccessData(res,'success',create_regional_adjustment);
            }

        }
    } catch (err) {
        response.sendErrorMessage(res, Constants.ERROR_500);
    }
};


const editUser=(data)=>{
    let dataToSave={};

    let criteria={
        id:data.id,
    }
    return new Promise((resolve,reject)=>{
        if(data){
            dataToSave=data;
        }
        query.update('User',criteria,dataToSave,{lean:true,new:true},(err,result)=>{
            if(err){
                reject(err);
            }else{
                resolve(result);
            }
        });
    })

};

const deleteUser = async (req, res) => {
    let criteria = {};
    if(req.params.id){
        criteria['id'] = req.params.id;
    }
    const company = await query.deleteById('User',criteria);
    response.sendsuccessData(res, 'Delete successfully', company);
}

const getUserList = async (req, res) => {
   
    query.getPopulateOne('User', req.params.id, {} ,{}, (err, result) => {
        if(err) response.sendErrorCustomMessage(res, "user Does Not Exist", 404)
        else response.sendsuccessData(res, 'User List fetched successfully', result);

    })
}


module.exports = {
    createUser,
    getUserDetails,
    getUserById,
    updateUser,
    deleteUser,
    getUserList
};