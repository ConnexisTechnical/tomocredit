'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const EcommerceSchema = new Schema({
    user : { type: Schema.Types.ObjectId, ref: 'User' },
    E_commerce:{
        Total_number_of_purchases:Number,
        Total_spent:Number,
        Average_purchase_size:Number,
        Category:String,
        Title:String,
        Condition: String,
        Seller:String,
        List_price:String,
        Purchase_price_per_unit:Number,
        Total_price:Number,
        Quantity:String,
        Payment_method_used:String,
        Shippin_zip_code:Number,

    },
  
    eCommerece_id:String,
    createdBy: String,
    created_at: {
        type: Date,
        default: Date.now(),
        es_indexed: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        es_indexed: true
    },


});

EcommerceSchema.set('toJSON', {
    getters: true,
    virtuals: true
});


module.exports = mongoose.model('E_Commerce', EcommerceSchema)
