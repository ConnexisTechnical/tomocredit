// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    require('./Bank');
    require('./Crypto');
    require('./E_Commerce');
    require('./Employee');
    require('./Investments');
    require('./Skills');

var userSchema = new Schema({
  email: String,
  password: String,
  fullname: String,
  created_at: { type: Date, default: Date.now },
  Bank : { type: Schema.Types.ObjectId, ref: 'Bank' },
  Crypto : { type: Schema.Types.ObjectId, ref: 'Crypto' },
  E_commerce : { type: Schema.Types.ObjectId, ref: 'E_Commerce' },
  Investment : { type: Schema.Types.ObjectId, ref: 'Investments' },
  Skills : { type: Schema.Types.ObjectId, ref: 'Skills' },
  Employee : { type: Schema.Types.ObjectId, ref: 'Employee' },
});

module.exports = mongoose.model('User', userSchema);
