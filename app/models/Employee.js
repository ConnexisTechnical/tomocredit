'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const EmployeeSchema = new Schema({
    user : { type: Schema.Types.ObjectId, ref: 'User' },
  
    employee_id:String,
    Employee:{
        Employer_name:String,
        Employer_location:String,
        industry:String,
        Position:String,
        Start_Date:String,

    },
    List_of_Previous_Employers:[{
        Employer_name:String,
        Employer_location:String,
        industry:String,
        Position:String,
        Start_Date:String,
    }],
    List_of_schools_attended:[{
        Name:String,
        Education_level:String,
        Location:String,
    }],
    Estimated_salary:Number,
    Connections:{
        Number_of_connections:Number,
        list_of_connection:[
            {
                Name:String,
                Location:String,
                company:String,
                Position:String,
                industry:String
            }
        ]
    },
    createdBy: String,
    created_at: {
        type: Date,
        default: Date.now(),
        es_indexed: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        es_indexed: true
    },


});

EmployeeSchema.set('toJSON', {
    getters: true,
    virtuals: true
});


module.exports = mongoose.model('Employee', EmployeeSchema)