'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const SkillsSchema = new Schema({
    user : { type: Schema.Types.ObjectId, ref: 'User' },
  
    skills_id:String,
    List_of_skills:[{
        skill_name:String,
    }],
    List_of_Media_Coverage:[{
        title:String,
        link:String,
    }],
    list_of_patents:[{
        title:String,
        status:String,
    }],
    createdBy: String,
    created_at: {
        type: Date,
        default: Date.now(),
        es_indexed: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        es_indexed: true
    },


});

SkillsSchema.set('toJSON', {
    getters: true,
    virtuals: true
});


module.exports = mongoose.model('Skills', SkillsSchema)