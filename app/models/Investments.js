'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const InvestmentSchema = new Schema({
    user : { type: Schema.Types.ObjectId, ref: 'User' },
  
    investement_id:String,
    total_balance:Number,
    number_of_investments:Number,
    Average_order_size:Number,
    List_of_investements_and_corresponding_size:[{
        Investment:String,
        Balance:Number,
    }],
    List_of_platform_and_corresponding_platfomrm:[{
        Platform:String,
        Balance:Number,
    }],
    List_of_places_checked_in:[{
        Location:String,
        Date:String,
    }],
    List_of_connected_application:[{
        App_name:String,
        Connected_date:String,
    }],
    List_of_ads_clicked:[{
        ad_name:String,
        ad_category:String
    }],
    
    createdBy: String,
    created_at: {
        type: Date,
        default: Date.now(),
        es_indexed: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        es_indexed: true
    },


});

InvestmentSchema.set('toJSON', {
    getters: true,
    virtuals: true
});


module.exports = mongoose.model('Investments', InvestmentSchema)