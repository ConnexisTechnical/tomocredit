'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const CryptoSchema = new Schema({
    user : { type: Schema.Types.ObjectId, ref: 'User' },
  
    crypto_id:String,
    Crypto:{
        total_balance:Number,
        number_of_investments:Number,
        Average_order_size:Number,
        List_of_assets_and_correspoding_balance:[{
            Assets:String,
            Count:String,
            Value_in_USD:Number,
        }],
        List_of_exchange_and_corresponding:[{
            Platfom:String,
            Balance:Number,
        }]
    },
    createdBy: String,
    created_at: {
        type: Date,
        default: Date.now(),
        es_indexed: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        es_indexed: true
    },


});

CryptoSchema.set('toJSON', {
    getters: true,
    virtuals: true
});


module.exports = mongoose.model('Crypto', CryptoSchema)