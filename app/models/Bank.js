'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const BankSchema = new Schema({
    user : { type: Schema.Types.ObjectId, ref: 'User' },
    bank_id:String,
    Total_account_balance:Number,
    Avg_account_balance_fot_the_6_month:Number,
    Estmated_salary:Number,
    Average_investment_per_month:Number,
    List_of_loan_payment:[{
        institution:String,
        Payment_amount:Number,
        Type:String,
    }],  
    createdBy: String,
    created_at: {
        type: Date,
        default: Date.now(),
        es_indexed: true
    },
    updated_at: {
        type: Date,
        default: Date.now,
        es_indexed: true
    },


});

BankSchema.set('toJSON', {
    getters: true,
    virtuals: true
});


module.exports = mongoose.model('Bank', BankSchema)