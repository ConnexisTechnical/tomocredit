const ERROR_WRONG = "404";
const ERROR_NOT_FOUND = "401";
const ERROR_500 = "500";
const ERROR_400 = "400";
const ERROR_404 = "404";
const SUCCESS = "200";
const SUCCESS_201 = "201";
const SUCCESS_202 = "202";
const ERROR_406 = "406";
const ERROR_304 = "304";
const ERROR_MESSAGE = "Oops,something went wrong";

module.exports = {
    ERROR_WRONG,
    ERROR_NOT_FOUND,
    SUCCESS,
    ERROR_MESSAGE,
    ERROR_500,
    ERROR_404,
    ERROR_400,
    ERROR_406,
    SUCCESS_201,
    SUCCESS_202,
    ERROR_304
};