
const express = require('express');
const router = express.Router();

// Require the controllers
const E_Commerce_controller = require('../controllers/E_Commerce');

router.post('/', E_Commerce_controller.createE_Commerce);
router.get('/', E_Commerce_controller.getE_CommerceDetails);
router.get('/:id', E_Commerce_controller.getE_CommerceById);


module.exports = router;

