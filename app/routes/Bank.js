const express = require('express');
const router = express.Router();

// Require the controllers
const Bank = require('../controllers/Bank');


router.post('/',Bank.createBank);
router.get('/',Bank.getBankDetails);
router.get('/:id',Bank.getBankById);

module.exports = router;

