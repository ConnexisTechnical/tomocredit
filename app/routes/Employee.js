const express = require('express');
const router = express.Router();

// Require the controllers
const Employee_controller = require('../controllers/Employee');

router.post('/', Employee_controller.createEmployee);
router.get('/', Employee_controller.getEmployeeDetails);
router.get('/:id', Employee_controller.getEmployeeById);


module.exports = router;