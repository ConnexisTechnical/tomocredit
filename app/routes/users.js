const express = require('express');
const router = express.Router();

// Require the controllers
const Users_controller = require('../controllers/User');

router.post('/', Users_controller.createUser);
router.get('/', Users_controller.getUserDetails);
router.get('/:id', Users_controller.getUserList);
router.put('/:id', Users_controller.updateUser);
router.delete('/:id', Users_controller.deleteUser);


module.exports = router;
