const express = require('express');
const router = express.Router();

// Require the controllers
const Skills_controller = require('../controllers/Skills');

router.post('/', Skills_controller.createSkills);
router.get('/', Skills_controller.getSkillsDetails);
router.get('/:id', Skills_controller.getSkillsById);


module.exports = router;
