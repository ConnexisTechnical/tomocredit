const express = require('express');
const router = express.Router();

// Require the controllers
const Investments_controller = require('../controllers/Investments');

router.post('/', Investments_controller.createInvestments);
router.get('/', Investments_controller.getInvestmentsDetails);
router.get('/:id', Investments_controller.getInvestmentsById);


module.exports = router;

