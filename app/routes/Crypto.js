const express = require('express');
const router = express.Router();

// Require the controllers
const Crypto_controller = require('../controllers/Crypto');

router.post('/', Crypto_controller.createCrypto);
router.get('/', Crypto_controller.getCryptoDetails);
router.get('/:id', Crypto_controller.getCryptoById);


module.exports = router;
    